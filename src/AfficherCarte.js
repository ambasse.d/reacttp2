import React,{ useState,useEffect } from 'react';
import axios from 'axios';
import "./AfficherCarte.css"

function AfficherCarte({nomCarte}){
    const[infoCarte, setInfoCarte] = useState(null);
    let cartes;
    let mesCartes;
    const [carteChosi,setCarteChoisi] = useState([])
    

    useEffect( () => {
        const fetch1 = async () => {
            if (infoCarte == null) {
            const resultCarte = await axios.get('https://los.ling.fr/cards');
            setInfoCarte(resultCarte.data);            
          }
        }
        fetch1(); 
    })
    function supprimerOnDecker(){
        return null
    }

    function addDecker(carte){
        console.log(carteChosi.length)
        setCarteChoisi([...carteChosi,carte]) 

        /***
         * J'ai voulu vérifier que si mon carte.id se trouve dans le useState carte
         * choisi elle ne fait rien sinon elle l'ajoute dans mon useState carteChoisi
         */
        /*if (carteChosi.length === 0 ){
            setCarteChoisi([...carteChosi,carte]) 
        }else{
            for(let i = 0 ; i<=carteChosi.length; i++){
                if (carteChosi[i][1] != carte.id){
                    setCarteChoisi([...carteChosi,carte]) 
                }
            }
        }*/     
    }
    console.log("mesCartes : ",carteChosi)

    if (carteChosi){

        mesCartes = carteChosi.map((carte) => {
            //console.log("id :",carte.id)
          
            return(
                
                <section className="col-md-4 carte">
                    <article className = "row-md topCarte" onClick={() =>addDecker(carte)}>
                        <img className = "img-fluid imageCarte" alt ="Carte League of Stone" src={"https://ddragon.leagueoflegends.com/cdn/img/champion/splash/"+carte.name+"_0.jpg"}/>
                        <p className = "m-1 text-white">Attaque : {carte.info.attack}</p>
                        <p className = "m-1 text-white">Defence : {carte.info.defense}</p>
                    </article>
                    <article className="row-md subCarte">          
                        <p className = "m-1 nameCarte">{carte.name}</p>   
                    </article>
                </section>
                
            )
        })
       }

    if (infoCarte){

        cartes = infoCarte.map((carte) => {
            //console.log("id :",carte.id)
          
            return(
                
                <section className="col-md-4 carte">
                    <article className = "row-md topCarte" onClick={() =>addDecker(carte)}>
                        <img className = "img-fluid imageCarte" alt ="Carte League of Stone" src={"https://ddragon.leagueoflegends.com/cdn/img/champion/splash/"+carte.name+"_0.jpg"}/>
                        <p className = "m-1 text-white">Attaque : {carte.info.attack}</p>
                        <p className = "m-1 text-white">Defence : {carte.info.defense}</p>
                    </article>
                    <article className="row-md subCarte">          
                        <p className = "m-1 nameCarte">{carte.name}</p>   
                    </article>
                </section>
                
            )
        })
       }
    return (
        <section className="row">
          <section className="col">
            <section className="container-md">
                <h1 className="">Champions des ligues</h1>
                <div className = "container-md row">
                    {cartes}
                </div>
            </section>
          </section>
          <section className ="col monDeck">
          <section className="container-md">
                <h1 className="">Mon deck</h1>
                <div className = "container-md row">
                    {mesCartes}
                </div>
            </section>
          </section>
          
          
        </section>  


    )/*
    return(
        <p>Hello</p>
    )*/
}
export default AfficherCarte