import './App.css';
import AfficherCarte from './AfficherCarte';


function App() {
  return (
    
    <div className="container-md">
        <h1 className = "title text-center pb-2">League of Stone</h1>   
        <AfficherCarte/> 
        
    </div>

  );
}

export default App;
